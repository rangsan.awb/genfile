import os

root = "data/test"
image_files = []

os.chdir(root)
#print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
for folder in os.listdir(os.getcwd()):
    #print("-------------------------------------------------------------")
    if os.path.isdir(folder): 
        os.chdir(os.path.join(os.getcwd(), folder))
        for filename in os.listdir(os.getcwd()):
            if filename.endswith(".jpg"):
                print(folder+"/"+filename)
                image_files.append("test"+"/"+folder+"/" + filename)    
        os.chdir("..")
os.chdir("..")

with open("test.txt", "w") as outfile:
    for image in image_files:
        outfile.write(image)
        outfile.write("\n")
    outfile.close()
os.chdir("..")